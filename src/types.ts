export enum PLAYER_TYPES {
    crosses = 'X',
    zeros = '0',
}
export const playerTypesNames = {
    [PLAYER_TYPES.crosses]: 'Крестики',
    [PLAYER_TYPES.zeros]: 'Нолики',
}

export const fieldSize = 10


export interface IField {
    size: number
}

export interface ICell {
    selectable: boolean
    win?: boolean
}

export interface IWinCells {
    rowIndex: number
    cellIndex: number
}

export interface IGridValues {
    value: PLAYER_TYPES | null,
    winPosition?: boolean,
}

export interface IStyledText {
    size: number,
}

