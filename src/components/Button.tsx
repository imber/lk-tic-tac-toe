import React from 'react'
import styled from 'styled-components'

interface IButton extends IStyledButton {
    label: string
    onClick: () => void
}

interface IStyledButton {
    disabled: boolean
}


export const StyledButton = styled.button<IStyledButton>`
  margin: 15px 15px 30px 15px;
  height: 50px;
  padding: 10px 15px;
  font-size: 16px;
  ${({ disabled }: IStyledButton) => disabled ? `
  background: rgb(220 220 220 / 51%);
  cursor: inherit;
  ` : `
  background:: gb(31 219 255 / 51%);
  cursor: pointer;
  `}
`;



const Button = (props: IButton) => {

    const { disabled, onClick } = props
    const handleOnClick = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
       onClick()
    };

    return <StyledButton disabled={disabled} onClick={!disabled ? handleOnClick : () => ({})} >{props.label}</StyledButton>

}

export default Button