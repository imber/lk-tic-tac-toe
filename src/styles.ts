import styled from "styled-components";
import {IField, ICell, IStyledText} from './types'

export const StyledText = styled.div<IStyledText>`
margin: 15px;
font-size: ${({ size }: IStyledText) => size}px
`
export const StyledSelect = styled.div`
margin: 15px;
width: 300px;
`

export const Row = styled.div`
display: flex;
flex-direction: row;
`

export const Cell = styled.div<ICell>`
width: 50px;
height: 50px;
border-bottom: 1px solid rgb(126 132 134);
border-left: 1px solid rgb(126 132 134);
${({ selectable }: ICell) => selectable && `
   cursor: pointer;
   &:hover {
        background: rgb(220 220 220 / 51%);
   }
`}
${({ win }: ICell) => win && `
   background: rgb(162 250 183 / 51%);
`}
`

export const Field = styled.div<IField>`
border-top: 1px solid rgb(126 132 134);
border-right: 1px solid rgb(126 132 134);
margin: 15px;
text-align: center;
  ${({ size }: IField) => `
        width: ${size*51}px;
  `}
`