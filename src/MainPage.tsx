import React, { useState } from 'react'
import Select, {OptionTypeBase as IOption} from 'react-select'
import Button from './components/Button'
import {StyledText, StyledSelect, Row, Cell, Field} from './styles'
import {PLAYER_TYPES, playerTypesNames, fieldSize, IWinCells, IGridValues} from './types'


const countForWinnerOptions: IOption[] = [
    { value: 3, label: '3' },
    { value: 5, label: '5' },
]

const getInitGridValues = (size: number) => {
    const row = [];
    const grid = [];
    for (let i = 1; i <= size; i++) {
        row.push({value: null});
    }
    for (let i = 1; i <= size; i++) {
        grid.push(row);
    }
    return grid
}

const MainPage = () => {

    const initCountForWinner: IOption = countForWinnerOptions[0]
    const initPlayerTurn = PLAYER_TYPES.crosses
    const [gridValues, setGridValues] = useState(getInitGridValues(fieldSize) as Array<Array<IGridValues>>)
    const [countForWinner, setCountForWinner] = useState(initCountForWinner)
    const [gameOn, setGameOn] = useState(false)
    const [gameOver, setGameOver] = useState(false)
    const [playerTurn, setPlayerTurn] = useState(initPlayerTurn as PLAYER_TYPES)
    const [stepCount, setStepCount] = useState(0)

    const resetState = () => {
        setGridValues(getInitGridValues(fieldSize))
        setPlayerTurn(initPlayerTurn)
        setCountForWinner(initCountForWinner)
        setGameOver(false)
        setStepCount(0)
    }

    const onClickCell = (rowIndex: number, cellIndex: number) => {
        if (!gameOver) {
            setStepCount(stepCount+1)
            const gridRow = gridValues[rowIndex]
            const gridCell = gridRow[cellIndex]
            if (gridCell.value !== null) {
                return
            } else {
                const newGrid = gridValues.slice()
                const newGridRow = gridRow.slice()
                newGridRow[cellIndex] = { value: playerTurn }
                newGrid[rowIndex] = newGridRow
                setGridValues(newGrid)
                checkGameIsOver(rowIndex, cellIndex)
                setPlayerTurn(playerTurn === PLAYER_TYPES.zeros ? PLAYER_TYPES.crosses : PLAYER_TYPES.zeros)
            }
        }
    }

    const saveWinCells = (winCells: IWinCells[]) => {
        const newGrid = gridValues.slice()
        winCells.map(cell => {
            const gridRow = newGrid[cell.rowIndex]
            const newGridRow = gridRow.slice()
            newGridRow[cell.cellIndex] = { value: playerTurn, winPosition: true }
            newGrid[cell.rowIndex] = newGridRow
        })
        setGridValues(newGrid)
    }

    const checkGameIsOver = (rowIndex: number, cellIndex: number) => {
        let winCells: IWinCells[] = [{rowIndex, cellIndex}]
        const calc = (rowIndex: number, cellIndex: number) => {
            winCells.push({rowIndex, cellIndex})
            winCellCount++
            if (winCellCount === countForWinner.value) {
                setGameOver(true)
                saveWinCells(winCells)
                return
            }
        }
        // по вертикали
        let winCellCount = 1
        // вниз
        for (let nextRow = rowIndex+1; nextRow < fieldSize && gridValues[nextRow][cellIndex].value === playerTurn; nextRow++) {
            calc(nextRow, cellIndex)
        }
        // вверх
        for (let nextRow = rowIndex-1; nextRow >= 0 && gridValues[nextRow][cellIndex].value === playerTurn; nextRow--) {
            calc(nextRow, cellIndex)
        }
        // по горизонтали
        winCells = [{rowIndex, cellIndex}]
        winCellCount = 1
        // вправо
        for (let nextCell = cellIndex+1; nextCell < fieldSize && gridValues[rowIndex][nextCell].value === playerTurn; nextCell++) {
            calc(rowIndex, nextCell)
        }
        // влево
        for (let nextCell = cellIndex-1; nextCell >= 0 && gridValues[rowIndex][nextCell].value === playerTurn; nextCell--) {
            calc(rowIndex, nextCell)
        }
        // по диагонали
        winCells = [{rowIndex, cellIndex}]
        winCellCount = 1
        // вправо вниз
        let nextRow = rowIndex+1
        let nextCell = cellIndex+1
        while (nextRow < fieldSize && nextCell < fieldSize && gridValues[nextRow][nextCell].value === playerTurn) {
            calc(nextRow, nextCell)
            nextRow++
            nextCell++
        }
        // влево вверх
        nextRow = rowIndex-1
        nextCell = cellIndex-1
        while (nextRow >= 0 && nextCell >= 0 && gridValues[nextRow][nextCell].value === playerTurn) {
            calc(nextRow, nextCell)
            nextRow--
            nextCell--
        }
        winCells = [{rowIndex, cellIndex}]
        winCellCount = 1
        // вправо вверх
        nextRow = rowIndex-1
        nextCell = cellIndex+1
        while (nextRow >= 0 && nextCell < fieldSize && gridValues[nextRow][nextCell].value === playerTurn) {
            calc(nextRow, nextCell)
            nextRow--
            nextCell++
        }
        // влево вниз
        nextRow = rowIndex+1
        nextCell = cellIndex-1
        while (nextRow < fieldSize && nextCell >= 0 && gridValues[nextRow][nextCell].value === playerTurn) {
            calc(nextRow, nextCell)
            nextRow++
            nextCell--
        }
        if (stepCount+1 >= fieldSize*fieldSize) {
            setGameOver(true)
        }
    }


    return (
        <div className="App">
            <StyledText size={24}>Игра крестики-нолики</StyledText>
            <StyledText  size={16}>Количество крестиков или ноликов в ряд, необходимых для выигрыша:</StyledText>
            <StyledSelect>
                <Select
                    options={countForWinnerOptions}
                    value={countForWinner}
                    onChange={setCountForWinner}
                    placeholder={'Выберете значение...'}
                    isDisabled={gameOn}
                />
            </StyledSelect>
            <Button
                label={!gameOn ? 'Начать' : 'Начать новую'}
                disabled={countForWinner === null || countForWinner === undefined}
                onClick={ () => {
                    gameOn && resetState()
                    setGameOn(!gameOn)
                } }
            />
            { gameOn && !gameOver && <StyledText size={20}>{`Ход игрока: ${playerTypesNames[playerTurn]}`}</StyledText> }
            { gameOn && gameOver && <StyledText  size={20}>{'Игра завершена'}</StyledText> }
            {gameOn &&
                <Field size={fieldSize}>
                    {gridValues.map((rows, rowIndex) => {
                        return (
                            <Row key={rowIndex}>
                                {rows.map((cell, cellIndex) => {
                                    return (
                                        <Cell
                                            key={cellIndex}
                                            onClick={() => onClickCell(rowIndex, cellIndex)}
                                            selectable={cell.value === null && gameOn && ! gameOver}
                                            win={cell.winPosition}
                                        >
                                            <StyledText size={18}>{cell.value && cell.value}</StyledText>
                                        </Cell>
                                    )
                                })}
                            </Row>
                        )
                     })
                    }
                </Field>
            }

        </div>
    );
}

export default MainPage;
